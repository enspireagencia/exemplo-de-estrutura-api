<?php

// require_once '../../src_vdc/user_conect.php';

class PixController {
  public function create($request)
  {
    $obj = json_decode($request);
    
    if (!isset($obj->form->name) || !isset($obj->form->cpf) || !isset($obj->form->value)) {
      http_response_code(422);
      return json_encode(['error' => "Erro de validação"]);
    }

    try {
        // $con = new Conexao();
        // $conexao = $con->conexao;
        // $stmt = $conexao->prepare("SELECT id_pedido, user_pedido, status_pedido, payment_method, qrcode, qrcode_image FROM pedidos WHERE id_pedido = ? AND user_pedido = ?");
        // $stmt->bind_param("ii", $obj->data->pedido, $obj->data->user);
        // $stmt->execute();
        // $stmt->bind_result($id, $user, $status, $method, $qrcode, $qrcodeimage);
        // $stmt->fetch();
        

        // $data = [
        // 'status' => $status,
        // 'method' => $method,
        // 'qrcode' => $qrcode,
        // 'qrcodeimage' => $qrcodeimage
        // ];
        
        // Monta o array com os concursos
        //Requisição ao banco
        //Estrutura o array
        $data = [
            [
                'title' => 'Nome do concurso',
                'status' => 'Status do concurso'
                'url' => 'URL do concurso'
            ],
            [
                'title' => 'Nome do concurso',
                'status' => 'Status do concurso'
                'url' => 'URL do concurso'
            ]
        ];
        return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    } catch (Exception $e) {
      print_r($e->getMessage());
    }
  }
}