<?php
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
$request = file_get_contents('php://input');

$routes = [
  '/' => 'ApiController@index',
  '/pix/create' => 'PixController@create',
];

$url = str_replace('/api', '', $_SERVER['REQUEST_URI']);

if (array_key_exists($url, $routes)) {
  $handler = $routes[$url];
  if (strpos($handler, '@') !== false) {
      list($class, $method) = explode('@', $handler);
      include $class.'.php';
      $controller = new $class();
      echo $controller->$method($request);
  } else {
      echo $handler();
  }
} else {
  echo "404";
}